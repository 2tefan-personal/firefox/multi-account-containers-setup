#!/bin/bash

##
# Color  Variables
##
red='\e[31m'
cyan='\e[36m'
clear='\e[0m'

c_start_entry="$red"
c_end_entry="$clear"

c_start_question="$cyan"
c_end_question="$clear"


: ${CURRENT_LOCATION:=$(dirname "$0")}
: ${PROFILES_DIR:="$CURRENT_LOCATION/profiles"}

profile_selected=""
destination_selected=""

invalid_option() {
    echo -ne "${c_start_entry}Invalid option.${c_end_entry}"
    exit 0
}

list_elements_and_select() {
    local use_basename=$1
    shift
    local arr_elements=("$@")
    
    i=1
    for element in "${arr_elements[@]}"
    do
        (( use_basename==1 )) && echo -ne "$c_start_entry$i)$c_end_entry\t$(basename "${element}")\n"
        (( use_basename==0 )) && echo -ne "$c_start_entry$i)$c_end_entry\t$element\n"
        
        ((i++))
    done
}

select_profile() {
    echo "Available profiles:"
    local profiles=("$PROFILES_DIR"/*)
    
    list_elements_and_select 1 "${profiles[@]}"
    
    echo -ne "$c_start_entry$i)$c_end_entry\tExit\n"
    echo -ne "${c_start_question}Select your desired profile:${c_end_question}\n"
    
    read num
    if ((1 <= num && num < i))
    then
        echo -ne "Using '${profiles[(($num-1))]}'\n\n\n"
        profile_selected="${profiles[(($num-1))]}"
    elif ((num == i))
    then
        exit 0
    else
        invalid_option
    fi
}

select_destination(){
    mapfile -t install_locations < <(find "$HOME/.waterfox" "$HOME/.mozilla" "$HOME/.firefox" "$HOME/.floorp" "$HOME/.librewolf" -name "containers.json" 2>/dev/null)
    
    list_elements_and_select 0 "${install_locations[@]}"
    echo -ne "${c_start_question}Select your desired destination:${c_end_question}\n"
    
    read num
    if ((1 <= num && num < i))
    then
        echo -ne "Using '${install_locations[(($num-1))]}'\n\n\n"
        destination_selected="${install_locations[(($num-1))]}"
    else
        invalid_option
    fi
}

yes_or_no() {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}


install() {
    source_containers="$profile_selected/containers.json"
    yes_or_no "Do you want to overwrite '$destination_selected' with '$source_containers'?" && echo "Proceeding..." || exit 1
    
    destination_selected_backup="${destination_selected}.bk"
    mv "$destination_selected" "$destination_selected_backup"
    cp "$source_containers" "$destination_selected"
    
    echo "Done :)"
    echo "Please install the extension settings via Firefox!"
}

select_profile
select_destination
install
