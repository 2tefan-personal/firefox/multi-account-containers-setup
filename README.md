# multi-account-containers-setup

Are you also a vivid Firefox Multi-Account Containers user? And are you annoyed that your container IDs change if they are synced via your Firefox Account? Then this is for you!

# 🚀 Usage
Just create your one profile in `./profiles` or use my personal profile. Then just run the script:
```sh
./setup.sh
```


# 🤔 Why should I care?
I use Firefox Multi-Account Containers to separate my [FAANG](https://www.urbandictionary.com/define.php?term=FAANG) accounts. So if I am in my Google/Alphabet container, it has no access to the cookies/LocalStorage for my Facebook/Meta container. (And yes I know, after Firefox released their [Total Cookie Protection](https://support.mozilla.org/en-US/kb/total-cookie-protection-and-website-breakage-faq) this would not really be needed anymore.)

Another nice thing of my Firefox setup is, that the default container is cleared automatically (and cookie popups are automatically hidden). So this means I can browse the web without having to click `decline` on each cookie popup, and I am also safe (citation needed), because all Cookies/LocalStorage/etc. are automatically cleared.

And sometimes I use them for their "actual" purpose: To login for a friend/family member without logging myself out of Google/...

## My extensions ⚙️

- [multi-account-containers](https://github.com/mozilla/multi-account-containers#readme) (duh 💁🏾)
- [Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete)
- [I don't care about cookies](https://www.i-dont-care-about-cookies.eu/)
- [Cookie Quick Manager](https://github.com/ysard/cookie-quick-manager) (Optional)
